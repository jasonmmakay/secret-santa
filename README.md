# Installation
Download santa.py and santa.config.

# Requirements

## Python
This script requires python 3. It was developed and tested against
[python 3.8.10](https://www.python.org/downloads/release/python-3810/);
no other versions were tested.

## SMTP Service
This script connects to and sends emails from an **S**imple **M**ail
**T**ransfer **P**rotocol server. It was developed and tested with Yahoo's
SMTP service in the past and Google's more recently. In theory, any SMTP
service should work, though I've had issues with both of these.


### Setting Up Google or Yahoo SMTP
1. Create/login to your Google or Yahoo mail account
2. Enable two-step verification
([Google](https://support.google.com/accounts/answer/185839)
/ [Yahoo](https://help.yahoo.com/kb/turn-yahoo-two-step-verification-sln6050.html))
3. Create and save an app password for santa.py
([Google](https://knowledge.workspace.google.com/kb/how-to-generate-an-app-passwords-000009237)
/ [Yahoo](https://help.yahoo.com/kb/access-yahoo-mail-third-party-apps-sln15241.html))\
**Be sure to save this password, you will not be able to retrieve it again after
it is created. But if you do lose it, just generate and use a new one.**
4. Modify the `[smtp]` section of `santa.config` for your service:

**Google**
```
[smtp]
address = smtp.gmail.com
port = 465
email = you@gmail.com
password = your-app-password
```

**Yahoo**
```
[smtp]
address = smtp.mail.yahoo.com
port = 587
email = you@yahoo.com
password = your-app-password
```

# Usage
```
usage: santa.py [-h] [--test-smtp TEST_SMTP] [--dryrun] [--force] [--visible] [config]

Run a Secret Santa lottery and send emails to each participant over SMTP.

positional arguments:
  config                An INI format configuration file relative to the working directory; defaults to santa.config, which contains documentation on its usage.

options:
  -h, --help            show this help message and exit
  --test-smtp TEST_SMTP
                        Test sending an email to the specified address via SMTP; all other options silently ignored
  --dryrun              Do everything except connect to an SMTP server and send emails
  --force               Force Santas to be reselected, despite a cache
  --visible             Print emails to screen, revealing secrets
```

See [santa.config](santa.config) for configuration details.

## Matching different people than last year
If you have your cache file available from last year, follow this process to
make sure that people are buying gifts for different people this year (assuming for config file is `santa.config`):

1. Show the secret Santa mapping from last year:\
execute `python santa.py santa.config --dryrun --visible`
2. Add the mappings to `[exclusions]` in `santa.config`
3. Choose new Santas for this year:\
execute `ptyhon santa.py santa.config --force`
