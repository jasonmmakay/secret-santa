import pickle
import smtplib
import ssl
import os
import argparse
import configparser
from email.message import EmailMessage
from random import shuffle
from sys import exit
from itertools import chain


def pick_santas(args, config):
    """
    Pair Santas with people to get gifts from them.

    Pairs are either loaded from cache or randomly selected if the cache does
    not exist or --force was specified on the command line. If the cache name
    is not specified in the config, `santa.cache` is used by default.

    Returns
    -------
    A dictionary mapping one person to another person.
    """
    # if we can use a cache and it was already created, load and return it
    cache_path = config.get('cache', 'name', fallback='santa.cache')
    if not args.force and os.path.exists(cache_path):
        with open(cache_path, 'rb') as f:
            people = pickle.load(f)
            print('loaded {}'.format(cache_path))
            return people

    if 'people' not in config:
        print('No people were specified in the config')
        exit(1)

    people_from = [*config['people'].keys()]
    people_to = people_from[:]

    # load exclusions; each key can have a space delimited list of names
    exclusions = set()
    if 'exclusions' in config:
        exclusions = {
            *chain.from_iterable(
                ((pf, pt) for pt in pts.split())
                for pf, pts in config['exclusions'].items())}

    # add exclusions so that no one is paired with themselves
    exclusions.update((p, p) for p in people_from)

    fixed = set()
    if 'fixed' in config:
        fixed = {
            (pf, pt) for pf, pt in config['fixed'].items()
            # don't include any pairings that aren't in the participant list
            if pf in config['people'] and pt in config['people']}

    # make sure there aren't any impossible combinations where a pairing is in
    # both the exclusion and fixed lists
    if fixed & exclusions:
        print('Pairings cannot be in both the exclusion and fixed lists')
        exit(1)

    # TODO: make sure no one has excluded all possible pairings

    # shuffle until the paired people don't contain any exclusions and all
    # fixed pairings are included
    # NOTE: on the first pass, each person is paired with themselves, causing
    #   the list to immediately be shuffled
    # TODO: this is hideously inefficient and O(random), but good enough if the
    #   exclusion and fixed lists aren't too dense, long, etc
    while True:
        # any(pair in exclusions for pair in zip(people_from, people_to))
        people_pairs = {*zip(people_from, people_to)}
        if exclusions & people_pairs or fixed - people_pairs:
            shuffle(people_to)
        else:
            break

    # cache the selections - this lets us resend emails later if needed without
    # reshuffling santas, for example, if an email address needs changed
    with open(cache_path, 'wb') as f:
        pickle.dump(people_pairs, f)
        print('saved {}'.format(cache_path))

    return people_pairs


def connect_to_smtp(args, config):
    """
    Connects to an SMTP server.

    Returns
    -------
    The SMTP server object and the email address to send the emails from.
    """
    smtp_address = config.get('smtp', 'address')
    smtp_port = config.getint('smtp', 'port')
    smtp_email = config.get('smtp', 'email')
    smtp_password = config.get('smtp', 'password')
    smtp_encryption = config.get('smtp', 'encryption')

    if (not smtp_address or not smtp_port
            or not smtp_email or not smtp_password):
        print(
            'An SMTP address, port, user and password must be in the [smtp]'
            ' section of your configuration file.')
        exit(1)

    try:
        if smtp_encryption.upper() == 'STARTTLS':
            server = smtplib.SMTP(smtp_address, smtp_port)
            context = ssl.create_default_context()
            server.starttls(context=context)

        elif smtp_encryption.upper() == 'SSL':
            server = smtplib.SMTP_SSL(smtp_address, smtp_port)

        else:
            print('smtp.encryption must be one of STARTTLS or SSL')
            exit(1)

        server.login(smtp_email, smtp_password)
        print('smtp connection successful')

        if args.dryrun:
            server.close()

    except Exception as ex:
        print('smtp connection failed: {}'.format(ex))
        exit(1)

    return server, smtp_email


def send_santa_emails(args, config, people):
    """Send emails to each Santa, over SMTP."""

    if args.visible:
        print('\nSanta mappings (from = to):')
        print(*(f'  {pf} = {pt}' for pf, pt in people), sep='\n')
        print('\n')

    # log into the email server
    server, smtp_email = connect_to_smtp(args, config)

    # get the email template and addresses to send them to
    subject_template = config.get(
        'email', 'subject',
        fallback='You are {receiver}\'s Secret Santa')
    body_template = config.get(
        'email', 'body',
        fallback='{santa}, choose a gift for {receiver}.')

    # get everyone's email addresses
    if 'people' not in config:
        print('No people were specified in the config')
        exit(1)

    emails = config['people']
    if missing_emails := [pf for pf, pt in people if pf not in emails]:
        print(
            'Some people are missing email addresses in the config:',
            *missing_emails, sep='\n')
        exit(1)

    # write and send the emails
    for pf, pt in people:
        msg = EmailMessage()
        msg.set_content(body_template.strip().format(santa=pf, receiver=pt))
        msg['Subject'] = subject_template.format(santa=pf, receiver=pt)
        msg['From'] = smtp_email
        msg['To'] = emails[pf]

        if args.visible:
            print(
                'FROM:    ', msg['From'],    '\n',
                'TO:      ', msg['To'],      '\n',
                'SUBJECT: ', msg['Subject'], '\n',
                'EMAIL BODY:\n',
                msg.get_content(),
                sep='')

        if not args.dryrun:
            try:
                server.send_message(msg)
                print('email sent to {}'.format(msg['To']))

            except Exception as ex:
                print(
                    'email failed to send to {}: {}'
                    .format(msg['To'], ex))

        elif not args.visible:
            # if this is a dryrun, but we didn't print the email to screen
            # above, print a message saying that an email will be sent
            print('email to be sent to {}'.format(msg['To']))

    if not args.dryrun:
        server.close()


def smtp_test_email(args, config):
    """Send a test email to make sure SMTP is working."""
    server, smtp_email = connect_to_smtp(args, config)

    msg = EmailMessage()
    msg.set_content('secret santa test email')
    msg['Subject'] = 'secret santa test email'
    msg['From'] = smtp_email
    msg['To'] = args.test_smtp

    try:
        server.send_message(msg)
        print('email sent to {}'.format(msg['To']))

    except Exception as ex:
        print(
            'email failed to send to {}: {}'
            .format(msg['To'], ex))

    server.close()


def main():
    parser = argparse.ArgumentParser(
        description='''
        Run a Secret Santa lottery and send emails to each participant over
        SMTP.
        ''')
    parser.add_argument(
        'config', nargs='?', default='santa.config',
        help='''
            An INI format configuration file relative to the working directory;
            defaults to %(default)s, which contains documentation on its usage.
            ''')
    parser.add_argument(
        '--test-smtp', type=str,
        help='''
            Test sending an email to the specified address via SMTP; all other
            options silently ignored
            ''')
    parser.add_argument(
        '--dryrun', action='store_true',
        help='Do everything except connect to an SMTP server and send emails')
    parser.add_argument(
        '--force', action='store_true',
        help='Force Santas to be reselected, despite a cache')
    parser.add_argument(
        '--visible', action='store_true',
        help='Print emails to screen, revealing secrets')
    args = parser.parse_args()

    config = configparser.ConfigParser()
    # make keys case-sensitive
    config.optionxform = lambda option: option
    config.read(args.config)

    if args.test_smtp is not None:
        # --dryrun is the only option that could affect this test, and we are
        # ignoring it, so set it to the default False
        if args.dryrun:
            args.dryrun = False

        smtp_test_email(args, config)
        return

    santas = pick_santas(args, config)
    send_santa_emails(args, config, santas)


if __name__ == '__main__':
    main()
